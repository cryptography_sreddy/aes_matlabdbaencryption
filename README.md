# CRYPTOGRAPHY->AES_MATLABDBAENCRYPTION

## This project allows a MATLAB application to encrypt the database credentials and at a later stage decrypt the stored-encrypted password for database access. 
A call is made to a C# ClassObject (.dll) file developed to perform the cryptograhic-functions. 
MATLAB, at the time, did not have any worthy encryption/ decryption functionality.

###### Branches: 
		1. 	Master: 	Completed, Tested, Verified
		2. 	Develop:	In progress, use at your own risk!
		
###### Usage:
		1. 	[C#][Visual Studio 2017]
		2. 	[System.Security.Cryptography][Namespace]
		3. 	[System.Runtime.InteropServices][Namespace]
		4. 	[MATLAB][MATLAB R2018a]
		
###### Solution: [AES.sln]
			aes: Windows-Form-Application (GUI) demo of AES256 using SecureString Textbox

###### Features:
		1. 	GUI to demo the Encryption and Decryption.
	
		2. 	Implementation of the SecureTextBox.
			This allows masking of password when being entered.
			This allows for the storage of an encrypted password and not a plaintext password. - very safe!
			
		3.  Includes Assembly file for interface & provide MATLAB with Cryptography Support 
		
		4.  MATLAB Script to demonstrate the interaction with the Assembly file to encrypt and decrypt passwords. 

#####Contact:
		sheldon@lucidworx.co.za